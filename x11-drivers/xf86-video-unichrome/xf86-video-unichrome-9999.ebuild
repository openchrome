# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit git x-modular

DESCRIPTION="Unichrome graphics driver for VIA/S3G cards"
HOMEPAGE="http://unichrome.sf.net"

EGIT_PROJECT="unichrome"
EGIT_REPO_URI="git://people.freedesktop.org/~libv/xf86-video-unichrome/"

KEYWORDS="-*"
IUSE="dri"

RDEPEND=">=x11-base/xorg-server-1.2
		x11-libs/libXvMC"

DEPEND="${RDEPEND}
	!x11-drivers/xf86-video-via
	x11-proto/fontsproto
	x11-proto/randrproto
	x11-proto/renderproto
	x11-proto/xextproto
	x11-proto/xproto
	dri? ( x11-proto/xf86driproto
		x11-proto/glproto
		x11-libs/libdrm
		x11-libs/libX11 )"

pkg_setup() {
	if use dri && ! built_with_use x11-base/xorg-server dri; then
		die "Build x11-base/xorg-server with USE=\"dri\"."
	fi
}

