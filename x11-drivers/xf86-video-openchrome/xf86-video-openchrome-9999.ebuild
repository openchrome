# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit x-modular subversion

DESCRIPTION="openChrome graphics driver for VIA/S3G cards"
HOMEPAGE="http://www.openchrome.org"

ESVN_PROJECT="openchrome-svn"
ESVN_BOOTSTRAP="eautoreconf"
# ESVN_REPO_URI="http://svn.openchrome.org/svn/trunk"


KEYWORDS="-*"
# waiting for info on
# http://wiki.openchrome.org/pipermail/openchrome-users/2007-October/003641.html
#IUSE="dri"
#CONFIGURE_OPTIONS="$(use_enable dri)"
IUSE="experimental xrandr"

RDEPEND=">=x11-base/xorg-server-1.2
		x11-libs/libXvMC"

DEPEND="${RDEPEND}
	x11-proto/fontsproto
	x11-proto/randrproto
	x11-proto/renderproto
	x11-proto/xextproto
	x11-proto/xproto
	x11-proto/xf86driproto
	x11-proto/glproto
	x11-libs/libdrm
	x11-libs/libX11"
#	dri? ( x11-proto/xf86driproto
#		x11-proto/glproto
#		x11-libs/libdrm
#		x11-libs/libX11 )"

src_unpack() {
	if use experimental; then 
		if use xrandr; then
			die "You can choose only one custom branch"
		fi
		einfo "Using experimental openChrome driver branch"
		export ESVN_REPO_URI="http://svn.openchrome.org/svn/branches/experimental_branch"
	else 
		if use xrandr; then
			einfo "Using xrandr openChrome driver branch"
			export ESVN_REPO_URI="http://svn.openchrome.org/svn/branches/randr_branch"
		else
			einfo "Using trunk openChrome driver branch"
			export ESVN_REPO_URI="http://svn.openchrome.org/svn/trunk"
		fi
	fi	
	subversion_src_unpack
}


pkg_setup() {
	# as we currently can't turn dri support off
	#if use dri && ! built_with_use x11-base/xorg-server dri; then
	if ! built_with_use x11-base/xorg-server dri; then
		die "Build x11-base/xorg-server with USE=\"dri\"."
	fi
}

